using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BillyController : MonoBehaviour
{
	public float speed;
	private Animator _anim;
	private float _moveInput;
	private bool facingRight = true;
	private Rigidbody2D _rb;
	void Start() {
		_anim = GetComponent<Animator>();
		_rb = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void FixedUpdate() {
		_moveInput = Input.GetAxis("Horizontal");

		//Thread.Sleep(1000);
		//if(_rb.position.y)
		_rb.MovePosition(_rb.position + Vector2.right * _moveInput * speed);
		//_rb.velocity = new Vector2(_moveInput * speed, _rb.velocity.y);
		if (facingRight == false && _moveInput < 0) {
			Flip();
		} else if (facingRight == true && _moveInput < 0) {
			Flip();
		}

		if (_moveInput == 0) {
			_anim.SetBool("isRuning", false);
		} else {
			_anim.SetBool("isRuning", true);
		}
	}
	void Flip() {
		facingRight = !facingRight;
		Vector3 scaler = transform.localScale;
		scaler.x *= -1;
		transform.localScale = scaler;

		if(_moveInput < 0) {
			transform.eulerAngles = new Vector3(0, 180, 0);
		}
		else if(_moveInput > 0) {
			transform.eulerAngles = new Vector3(0, 0, 0);
		}
	}
}
